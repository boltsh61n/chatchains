import itertools

from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from V2.chains import MainChain, Chain, listener
from V2.handlers import ChainHandler
from V2.context import ChainContext

calculator_keyboard = [["1", "2", "3", "+"],
                       ["4", "5", "6", "-"],
                       ["7", "8", "9", "*"],
                       ["0", "/", "=", "C"]]
calculator_symbols = list(itertools.chain.from_iterable(calculator_keyboard))


def kb(keyboard_list):
    inline_keyboard = []
    for line in keyboard_list:
        inline_keyboard_line = []
        for btn in line:
            inline_keyboard_line.append(InlineKeyboardButton(text=btn, callback_data="/" + btn))
        inline_keyboard.append(inline_keyboard_line)
    return InlineKeyboardMarkup(inline_keyboard=inline_keyboard)


class CalculatorChain(Chain):
    @property
    def text(self):
        line = self.cache("line")
        return line if line else "..."

    @property
    def keyboard(self):
        return kb(calculator_keyboard)

    @listener()
    def handle(self, text=None, callback=None, **kwargs):
        inp = text if text and text in calculator_symbols else callback[1:] if callback else None

        if inp:
            line = self.cache("line", default="")

            if inp == "=":
                try:
                    res = eval(line)
                except:
                    res = "value_error"
                line += f"\n>>> {res}"
            elif inp == "C":
                line = ""
            else:
                line += inp

            self.cache("line", line)


class CalculatorMainChain(MainChain):
    inner_chains = [CalculatorChain]

    @property
    def text(self):
        return "Welcome to calculator bot"

    @property
    def keyboard(self):
        return InlineKeyboardMarkup(
            inline_keyboard=[[
                InlineKeyboardButton(text="start calculating", callback_data="/calculator"),
                InlineKeyboardButton(text="about", callback_data="/about")
            ]])

    @listener(callback="/about")
    def about(self, **kwargs):
        self.pop("created by shlomy boltin", 5)

    @listener(callback="/calculator")
    def calculator(self, **kwargs):
        self.start_inner("CalculatorChain")


context = ChainContext(token="2145879773:AAH4lax_OsaJQe8Mraadq0lYxiTc_fl3kkQ")
bot_handler = ChainHandler(context, CalculatorMainChain)
bot_handler.run()
