import itertools

from ...V2.context import ChainContext
from ...V2.handlers import ChainHandler
from ...V2.chains import listener
from ...V2.chains import MainChain

calculator_keyboard = [["1", "2", "3", "+"],
                       ["4", "5", "6", "-"],
                       ["7", "8", "9", "*"],
                       ["0", "C", "/", "="]]
calculator_symbols = list(itertools.chain.from_iterable(calculator_keyboard))


class CalculatorChain(MainChain):
    @property
    def text(self):
        line = self.cache("line")
        return line if line else ">>>"

    @property
    def keyboard(self):
        return self.keyboard_builder(calculator_keyboard)

    @listener()
    def handle(self, text, callback):
        inp = text if text and text in calculator_symbols else callback[1:] if callback else None

        if inp:
            line = self.cache("line", default="")

            if inp == "=":
                try:
                    line = line.split(">>>")[-1]
                    res = eval(line)
                except:
                    res = "value_error"
                line += f"\n>>> {res}"
            elif inp == "C":
                line = ""
            else:
                line += inp

            self.cache("line", line)
            self.refresh()

        else:
            self.refresh()


# context = ChainContext(token="2145879773:AAH4lax_OsaJQe8Mraadq0lYxiTc_fl3kkQ")
#
# bot_handler = ChainHandler(context, CalculatorChain)
# bot_handler.run()
