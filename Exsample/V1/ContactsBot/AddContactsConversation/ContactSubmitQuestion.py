from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from V1.Question.QuestionBase import Question


class ContactSubmitQuestion(Question):

    def ask(self, chat_id):
        self.set_page(chat_id,
                          f"Your new contact details are\n"
                          f"name: {self.cache(chat_id, 'new_contact_name')}\n"
                          f"phone number: {self.cache(chat_id, 'new_contact_number')}\n"
                          f"would you like to save it?",
                          InlineKeyboardMarkup(inline_keyboard=[
                              [InlineKeyboardButton(text="yes", callback_data="/yes")],
                              [InlineKeyboardButton(text="no", callback_data="/no")]
                          ]
                          ))

    def handle_input(self, chat_id, message: str, callback, files=None):
        if callback:
            if callback == "/yes":
                contacts_list = self.cache(chat_id, "contacts_list")
                if contacts_list is None:
                    contacts_list = []
                contacts_list.append({
                    "name": self.cache(chat_id, "new_contact_name"),
                    "number": self.cache(chat_id, "new_contact_number")
                })
                self.cache(chat_id, "contacts_list", contacts_list)

                self.set_page(chat_id, "contact saved", sleep=1)
                self.main_conversation.show(chat_id)

            elif callback == "/no":
                self.set_page(chat_id, "operation canceled", sleep=1)
                self.main_conversation.show(chat_id)
