from V1.Question.QuestionBase import Question


class ContactNameQuestion(Question):
    def ask(self, chat_id):
        self.set_page(chat_id, "what is the contacts name?")

    def handle_input(self, chat_id, message: str, callback, files=None):
        if message:
            self.cache(chat_id, "new_contact_name", message)
            self.father.start_inner(chat_id, "ContactNumberQuestion")
