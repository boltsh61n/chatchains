from V1.Conversation.ConversationBase import Conversation
from Exsample.V1.ContactsBot.AddContactsConversation.ContactNameQuestion import ContactNameQuestion
from Exsample.V1.ContactsBot.AddContactsConversation.ContactNumberQuestion import ContactNumberQuestion
from Exsample.V1.ContactsBot.AddContactsConversation.ContactSubmitQuestion import ContactSubmitQuestion


class AddContactsConversation(Conversation):
    inner_objects = [
        ContactNameQuestion,
        ContactNumberQuestion,
        ContactSubmitQuestion
    ]

