from V1.Question.QuestionBase import Question


def is_phone_number(text):
    if len(text) == 10:
        return True
    return False


class ContactNumberQuestion(Question):
    def ask(self, chat_id):
        self.set_page(chat_id, "What is the number?")

    def handle_input(self, chat_id, message: str, callback, files=None):
        if message:
            if is_phone_number(message):
                self.cache(chat_id, "new_contact_number", message)
                self.father.start_inner(chat_id, "ContactSubmitQuestion")

            else:
                self.send_message(chat_id, "This doesnt look like an phone number, try again")




