from V1.Conversation.MainConversationBase import MainConversationBase
from Exsample.V1.ContactsBot.AddContactsConversation.AddContactsConversation import AddContactsConversation
from Exsample.V1.ContactsBot.MainQuestion import MainQuestion


class MainConversation(MainConversationBase):
    inner_objects = [
        MainQuestion,
        AddContactsConversation
    ]
