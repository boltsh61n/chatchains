import telepot

from V1.ContextBase import ChainContext
from Exsample.V1.ContactsBot.MainConversation import MainConversation

# TOKEN = ""
TOKEN = input("Enter bot token.\n>>>")
offline_test = False

context = ChainContext(telepot.Bot(TOKEN), offline_test)
main_conversation = MainConversation(context=context)

main_conversation.run()
