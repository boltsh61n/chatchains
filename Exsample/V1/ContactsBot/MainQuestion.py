from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from V1.Question.QuestionBase import Question


class MainQuestion(Question):

    def ask(self, chat_id):
        self.set_page(chat_id, "hello and welcome to contacts bot\n"
                               " what would you like to do?",
                      InlineKeyboardMarkup(inline_keyboard=[
                          [InlineKeyboardButton(text="add contact", callback_data="/addContact")],
                          [InlineKeyboardButton(text="view contacts", callback_data="/viewContacts")]
                      ]))

    def handle_input(self, chat_id, message: str, callback, files=None):
        if callback:
            if callback == "/addContact":
                self.father.start_inner(chat_id, "AddContactsConversation")

            if callback == "/viewContacts":
                contacts_list = self.cache(chat_id, "contacts_list")

                if contacts_list:
                    contacts_message = "Contacts list\n"
                    for contact in contacts_list:
                        print(contact)
                        contacts_message += f"name: {contact['name']}\n" \
                                            f"phone number: {contact['number']}\n\n"

                    self.set_page(chat_id, contacts_message, sleep=3, restore_after_sleep=True)
                else:
                    self.set_page(chat_id, "no contacts", sleep=3, restore_after_sleep=True)
