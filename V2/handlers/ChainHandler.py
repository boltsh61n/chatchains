import time
import traceback
from pprint import pprint

from telepot import exception
from urllib3.exceptions import ProtocolError, NewConnectionError, MaxRetryError
from threading import Thread

from ..context.ChainContext import ChainContext
from ..chains.main_chain_base.main_chain import MainChain


class ChainHandler(Thread):
    CONTENT_TYPES = ["voice", "photo", "video", "video_note"]

    def __init__(self, context: ChainContext, main_chain: type(MainChain)):
        super().__init__()
        self.context = context
        self.main_chain_class = main_chain
        self.running = True
        self.is_online = True

    def update_handler(self, update: dict):
        pprint(update)
        if update.get("message"):
            self.handle_message(update.get("message"))
        elif update.get("callback_query"):
            self.handle_callback(update['callback_query'])

    def handle_message(self, message):
        chat_id = message['chat']['id']

        # Extracting content from message (text for text, file id for media)
        text = message.get("caption") if message.get("caption") else message.get("text")
        media_type, media = next(((x, message.get(x)) for x in self.CONTENT_TYPES if message.get(x)),
                                 (None, None))
        kwargs = {}
        if text: kwargs['text'] = text
        if media_type: kwargs[media_type] = media

        if kwargs:
            self.pass_to_chain(chat_id, **kwargs)
            return
        # delete messages todo
        print("Unhandled message", message)

    def handle_callback(self, callback):
        chat_id = callback['message']['chat']['id']
        self.pass_to_chain(chat_id, callback=callback['data'])

    def pass_to_chain(self, chat_id, **kwargs):
        try:
            main_chain: MainChain = self.main_chain_class(chat_id=chat_id, context=self.context)
            main_chain.handle_input(**kwargs)

        except (ConnectionResetError, ProtocolError, NewConnectionError, MaxRetryError):
            pass

        except Exception:
            traceback.print_exc()
            print(self.main_chain_class.__name__)
            main_chain: MainChain = self.main_chain_class(chat_id=chat_id, context=self.context)

            main_chain.pop(text="Oops it looks like the bot is having trouble dealing with your input\n"
                                "try something else or try again later", sleep=10)

    def run(self):
        if self.context.settings.testing_mode:
            test_chat_id = 99999999

            while True:
                message = input(">>>")
                if message.startswith("/"):
                    self.pass_to_chain(test_chat_id, callback=message)
                else:
                    self.pass_to_chain(test_chat_id, text=message)

        else:
            print('Listening ...')
            while 1:
                if self.running:
                    self.updates_loop()

                self.is_online = False
                time.sleep(2)

    def updates_loop(self, relax=0.1, offset=None, timeout=5, allowed_updates=None, quiet=False):

        while self.running:
            try:
                result = self.context.bot.getUpdates(offset=offset,
                                                     timeout=timeout,
                                                     allowed_updates=allowed_updates)
                self.is_online = True

                # Once passed, this parameter is no longer needed.
                allowed_updates = None

                # No sort. Trust server to give messages in correct order.
                for update in result:
                    self.update_handler(update)
                    offset = update['update_id'] + 1

            except exception.BadHTTPResponse as e:
                if not quiet:
                    traceback.print_exc()

                # Servers probably down. Wait longer.
                if e.status == 502:
                    time.sleep(30)

                self.is_online = False

            except(MaxRetryError, ProtocolError, NewConnectionError, ConnectionResetError):
                self.is_online = False

            except:
                if not quiet:
                    traceback.print_exc()
            finally:
                time.sleep(relax)

    def pause(self):
        self.running = False

    def resume(self):
        self.running = True
