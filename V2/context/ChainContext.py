import telepot

from .Cache import Cache


class ChainContext:
    class settings:
        token = None
        testing_mode = False

    def __init__(self, token, testing_mode=False):
        self.settings.token = token
        self.settings.testing_mode = testing_mode

        self.cache = Cache()

    @property
    def bot(self):
        return telepot.Bot(self.settings.token)
