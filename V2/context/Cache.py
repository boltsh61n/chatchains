class Cache:

    def __init__(self):
        self.__data = {}

    def set(self, chat_id, key, value):

        self.user_cache(chat_id)[key] = value
        return self.user_cache(chat_id).get(key)

    def get(self, chat_id, key, default=None):
        value = self.user_cache(chat_id).get(key)
        return default if value is None else value

    def set_system_cache(self, chat_id, key, value):
        self.system_cache(chat_id)[key] = value
        return self.system_cache(chat_id)[key]

    def get_system_cache(self, chat_id, key, default=None):
        value = self.system_cache(chat_id).get(key)
        return default if value is None else value

    def user_cache(self, chat_id):
        temp_cache_key = "chat-temp-cache"
        user_cache_key = f"{chat_id}-chat"

        if not self.__data.get(temp_cache_key):
            self.__data[temp_cache_key] = {}

        if not self.__data[temp_cache_key].get(user_cache_key):
            self.__data[temp_cache_key][user_cache_key] = {}

        return self.__data[temp_cache_key][user_cache_key]

    def system_cache(self, chat_id):
        temp_cache_key = "chat-temp-cache"
        user_system_cache_key = f"{chat_id}-system"

        if not self.__data.get(temp_cache_key):
            self.__data[temp_cache_key] = {}

        if not self.__data[temp_cache_key].get(user_system_cache_key):
            self.__data[temp_cache_key][user_system_cache_key] = {}

        return self.__data[temp_cache_key][user_system_cache_key]
