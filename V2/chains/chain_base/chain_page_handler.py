import time
from copy import copy

from .chain_part_base import ChainPart


class PageHandler(ChainPart):
    def __init__(self, chat_id, context):

        super().__init__(chat_id, context)
        self.inner_pages = []

    def update_page(self, text=None, keyboard=None, sleep=None):
        text = text if text else self.text
        keyboard = keyboard if keyboard else self.keyboard

        if self.settings.testing_mode:
            return self.send_message(self.chat_id, text, keyboard)

        current_message = self.cache("current_message", default={})

        if current_message.get("id"):
            message_identifier = (self.chat_id, current_message['id'])

            if text and text != current_message.get("text"):
                self.bot.editMessageText(message_identifier, text, reply_markup=keyboard)

            elif keyboard and keyboard != current_message.get("keyboard"):
                self.bot.editMessageReplyMarkup(message_identifier, reply_markup=keyboard)

        else:
            res = self.send_message(self.chat_id, text, keyboard)
            current_message['id'] = res['message_id']

        current_message['text'] = text if text else current_message.get("text")
        current_message['keyboard'] = keyboard if keyboard else current_message.get("keyboard")

        self.cache("current_message", current_message)

        if sleep:
            time.sleep(sleep)

    def pop(self, text, sleep=3):
        old_message = copy(self.cache("current_message", default={}))
        self.update_page(text=text, sleep=sleep)
        if old_message:
            self.update_page(old_message.get("text"), old_message.get("keyboard"))

    def add_page(self, chat_id, name, page):
        self.inner_pages.append(page)

    def send_message(self, chat_id, text, keyboard):
        return self.bot.sendMessage(chat_id, text, reply_markup=keyboard)
