from __future__ import annotations

from ...context import ChainContext
from .chain_page_handler import PageHandler
from .chain_event_handler import EventHandler


class Chain(EventHandler, PageHandler):

    def __init__(self, father: Chain, **kw):

        self.father = father
        self.chat_id = father.chat_id
        self.context: ChainContext = father.context

        super(Chain, self).__init__(self.chat_id, self.context)

        self.__inner_chains = {}
        for chain_class in self.inner_chains:
            chain: Chain = chain_class(self)
            self.__inner_chains[chain.id] = chain

    # chain operations functions
    def show(self):
        self.state = self.path

        if self.text:
            self.update_page(self.text, self.keyboard)
        elif self.__inner_chains:
            list(self.__inner_chains.values())[0].show()
        else:
            # raise empty chain
            print("empty chain")

    def start_inner(self, inner_id):
        if inner_id in self.__inner_chains.keys():
            self.__inner_chains[inner_id].show()
        else:
            raise Exception(f"Cant find chain named {inner_id} in {self.id}")

        # raise not found

    def handle_input(self, **kwargs):
        if self.state:
            return self.__inner_chains[self.state].handle_input(**kwargs)

        else:
            self.run_event_handlers(**kwargs)
            self.refresh()

    # additional values
    @property
    def path(self) -> list:
        """
            returns list of chain ids from chain and all chains above in hierarchy
        """

        if self.father:
            return self.father.path + [self.id]

        return [self.id]

    @property
    def state(self):
        """
            returns chain id of the activated inner chain. if no active chain, returns None
        """
        full_state: list = self.system_cache("user_state", default=[])

        if full_state:
            state = full_state[len(self.path):]
            if state:
                return state[0]

    @state.setter
    def state(self, state):
        self.system_cache("user_state", state)

    @property
    def id(self):
        return str(self).split(" ")[0].split(".")[-1]

    def refresh(self):
        if not self.state:
            self.update_page()


