from .chain_part_base import ChainPart


def listener(text=None, callback=None, voice=None, photo=None, video=None, video_note=None, block_others=True,
             collector=False):
    def inner_dec(func):
        func.run_if_text = text
        func.run_if_callback = callback
        func.run_if_voice = voice
        func.run_if_photo = photo
        func.run_if_video = video
        func.run_if_video_note = video_note

        func.has_filters = any([
            func.run_if_text,
            func.run_if_callback,
            func.run_if_voice,
            func.run_if_photo,
            func.run_if_video,
            func.run_if_video_note])

        func.block_others = block_others
        func.collector = collector
        func.is_handler = True
        return func

    return inner_dec


class EventHandler(ChainPart):

    # event handlers handlers
    def get_handlers(self):
        h_funcs = []
        c_funcs = []
        for maybeHandler in self.__dir__():
            if not maybeHandler.startswith("__"):
                func = getattr(self, maybeHandler)
                if hasattr(func, 'is_handler'):
                    c_funcs.append(func) if func.collector else h_funcs.append(func)

        return h_funcs + c_funcs

    def run_event_handlers(self, **kwargs):
        text, callback, voice, photo, video, video_note = self.unpack_update(kwargs)
        block_others = False
        for handler in self.get_handlers():

            if ((not block_others) and
                    (not handler.has_filters or
                     text and (text == handler.run_if_text) or
                     callback and (callback == handler.run_if_callback) or
                     voice and handler.run_if_voice or
                     photo and handler.run_if_photo or
                     video and handler.run_if_video or
                     video_note and handler.run_if_video_note)):
                handler(**kwargs)
                if handler.block_others:
                    block_others = True

    @staticmethod
    def unpack_update(kwargs: dict):
        text = kwargs.get("text")
        callback = kwargs.get("callback")
        photo = kwargs.get("photo")
        video = kwargs.get("video")
        voice = kwargs.get("voice")
        video_note = kwargs.get("video_note")
        return text, callback, voice, photo, video, video_note
