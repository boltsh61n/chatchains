from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from ...context import ChainContext


class ChainPart(object):
    def __init__(self, chat_id, context: ChainContext, **kw):
        self.chat_id = chat_id
        self.context = context

    # chain variables
    inner_chains = []

    @property
    def text(self):
        return None

    @property
    def keyboard(self):
        return None

    # helpful functions
    @staticmethod
    def keyboard_builder(keyboard_list):
        inline_keyboard = []
        for line in keyboard_list:
            inline_keyboard_line = []
            for btn in line:
                if type(btn) == type(("", "")):
                    text, callback = btn
                    inline_keyboard_line.append(InlineKeyboardButton(text=text, callback_data="" + callback))
                else:
                    inline_keyboard_line.append(InlineKeyboardButton(text=btn, callback_data="/" + btn))
            inline_keyboard.append(inline_keyboard_line)
        return InlineKeyboardMarkup(inline_keyboard=inline_keyboard)

    # shortcuts to context
    def cache(self, key, value=None, default=None):

        if value is not None:
            self.context.cache.set(self.chat_id, key, value)

        return self.context.cache.get(self.chat_id, key, default=default)

    def system_cache(self, key, value=None, default=None):
        if value:
            self.context.cache.set_system_cache(self.chat_id, key, value)

        return self.context.cache.get_system_cache(self.chat_id, key, default=default)

    @property
    def settings(self):
        return self.context.settings

    @property
    def bot(self):
        return self.context.bot
