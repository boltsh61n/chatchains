from ...chains import Chain
from ...chains import listener


class MainChain(Chain):
    def __init__(self, chat_id, context):
        self.chat_id = chat_id
        self.context = context

        super().__init__(self)
        self.father = None

    @listener(collector=True)
    def collect_all(self, **kwargs):
        self.show()
