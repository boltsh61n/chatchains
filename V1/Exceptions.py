class NoTelegramHandler(Exception):
    pass


class CantStartConversation(Exception):
    pass


class InnerObjectNotFound(Exception):
    pass
