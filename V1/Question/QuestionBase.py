from copy import deepcopy

from V1.InnerObject.InnerObject import InnerObject


class Question(InnerObject):
    object_type = "Question"

    def start(self, chat_id):
        self.set_father_stage(chat_id)
        self.ask(chat_id)

    def ask(self, chat_id):
        self.father.send_message(chat_id, f"asking about {self.id}")

    def handle_input(self, chat_id, message: str, callback, files=None):
        self.father.send_message(chat_id, f"handling answer from {self.id}")

    @staticmethod
    def format_btn(btn: dict, *args, **kwargs):
        btn = deepcopy(btn)
        if btn.get("text"):
            btn['text'] = btn['text'].format(*args, **kwargs)

        if btn.get("callback_data"):
            btn['callback_data'] = btn['callback_data'].format(*args, **kwargs)

        if btn.get("url"):
            btn['url'] = btn['url'].format(*args, **kwargs)

        return btn
