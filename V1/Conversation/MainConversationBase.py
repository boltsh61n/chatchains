from threading import Thread

from telepot.loop import GetUpdatesLoop

from V1.Conversation.ConversationBase import Conversation


class MainConversationBase(Conversation, Thread):

    def update_handler(self, update):
        if update.get("message"):
            self.handle_msg(update['message'])
        elif update.get("callback_query"):
            self.handle_callbacks(update['callback_query'])
        else:
            print("Unhandled update", update)

    def handle_msg(self, msg):
        chat_id = msg['chat']['id']

        if msg.get("text"):
            self.handle_input(chat_id, message=msg['text'])

        self.delete_message(chat_id, msg)

    def handle_callbacks(self, callback):
        chat_id = callback['message']['chat']['id']
        self.handle_input(chat_id, callback=callback['data'])

    def run(self):
        if self.context.testing_mode:
            test_chat_id = 99999999

            while True:
                message = input(">>>")
                if message.startswith("/"):
                    self.handle_input(test_chat_id, callback=message)
                else:
                    self.handle_input(test_chat_id, message)

        else:
            print('Listening ...')
            update_loop = GetUpdatesLoop(self.bot, self.update_handler)

            update_loop.run_forever()

    def run_on_input(self, chat_id, full_message=None, callback=None, files=None):
        if self.get_stage(chat_id) is None:
            self.start(chat_id)
