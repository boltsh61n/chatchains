from V1.ContextBase import ChainContext
from V1.InnerObject.InnerObject import InnerObject
from V1.Exceptions import *


class Conversation(InnerObject):
    object_type = "Conversation"
    inner_objects = []

    def __init__(self, father=None, context: ChainContext = None):
        self.stage = {}
        super().__init__(father, context=context)

        # Initialising inner objects
        inner_objects = []
        for inner_object in self.inner_objects:
            inner_object: type(InnerObject)
            inner_objects.append(inner_object(self))

        self.inner_objects = inner_objects

    def start(self, chat_id):
        if not self.inner_objects:
            raise CantStartConversation("Cant start conversation without inner conversations or questions")

        self.set_father_stage(chat_id)
        self.start_inner(chat_id, self.inner_objects[0].id)

    def end(self, chat_id):
        self.father.show(chat_id)

    def start_inner(self, chat_id, inner_id):
        self.get_inner_object(inner_id).show(chat_id)

    def handle_input(self, chat_id, message=None, callback=None, files=None, full_message=None):

        stage = self.get_stage(chat_id)
        if stage:
            for inner_object in self.inner_objects:
                if stage == inner_object.id:
                    inner_object.handle_input(chat_id, message, callback, files=files)

        self.run_on_input(chat_id, full_message, callback, files)

    def get_stage(self, chat_id):
        return self.stage.get(chat_id)

    def set_stage(self, chat_id, stage):
        self.stage[chat_id] = stage

    def clear_stage(self, chat_id):
        self.stage[chat_id] = None

    def get_inner_object(self, inner_id):
        for inner_object in self.inner_objects:
            if inner_object.id == inner_id:
                return inner_object
        raise InnerObjectNotFound(f"Cant fine inner object named {inner_id} in {self.id}")

    def run_on_input(self, chat_id, full_message=None, callback=None, files=None):
        pass
