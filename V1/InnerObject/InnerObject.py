import time
from copy import copy

import telepot

from V1.ContextBase import ChainContext
from V1.Exceptions import NoTelegramHandler
from V1.InnerObject.InnerObjectOverwrites import InnerObjectOverwrites


class InnerObject(InnerObjectOverwrites):

    def __init__(self, father, context: ChainContext = None):
        self.father = father

        if father:
            self.father = father
            self.context = father.context

        else:
            if not context:
                raise NoTelegramHandler("Missing telegram handler in", self.id)

            self.context = context

    def send_message(self, chat_id, message, keyboard=None):
        if self.context.testing_mode:
            print(message)
            if keyboard:
                for row in keyboard.inline_keyboard:
                    for callback in row:
                        print(callback.text, callback.callback_data)
            print("callbacks: ", )
            return
        return self.bot.sendMessage(chat_id, message, reply_markup=keyboard)

    def delete_message(self, chat_id, message=None, message_id=None):
        if message:
            message_id = message['message_id']
        if message_id:
            self.bot.deleteMessage((chat_id, message_id))

    def set_page(self, chat_id, text=None, keyboard=None, sleep=None, restore_after_sleep=False):
        if self.context.testing_mode:
            return self.send_message(chat_id, text, keyboard)

        current_message = self.cache(chat_id, "current_message")
        current_message = current_message if current_message else {}

        print(current_message)
        old_message = copy(current_message)

        if current_message.get("id"):
            message_identifier = (chat_id, current_message['id'])

            if text and text != current_message.get("text"):
                if keyboard and keyboard != current_message.get("text"):
                    self.bot.editMessageText(message_identifier, text, reply_markup=keyboard)
                else:
                    self.bot.editMessageText(message_identifier, text)

            elif keyboard and keyboard != current_message.get("keyboard"):
                self.bot.editMessageReplyMarkup(message_identifier, reply_markup=keyboard)
        else:
            res = self.send_message(chat_id, text, keyboard)
            current_message['id'] = res['message_id']

        current_message['text'] = text if text else current_message.get("text")
        current_message['keyboard'] = keyboard if keyboard else current_message.get("keyboard")

        self.cache(chat_id, "current_message", current_message)

        if sleep:
            time.sleep(sleep)
            if restore_after_sleep and old_message:
                self.set_page(chat_id, old_message.get("text"), old_message.get("keyboard"))

    def cache(self, chat_id, key, value=None):
        temp_cache_key = "temp-cache"
        user_cache_key = f"{chat_id}-temp"
        if not self.context.cache.get(temp_cache_key):
            self.context.cache[temp_cache_key] = {}

        if not self.context.cache[temp_cache_key].get(user_cache_key):
            self.context.cache[temp_cache_key][user_cache_key] = {}

        if value:
            self.context.cache[temp_cache_key][user_cache_key][key] = value

        if self.context.cache[temp_cache_key][user_cache_key].get(key):
            return self.context.cache[temp_cache_key][user_cache_key][key]

    def set_father_stage(self, chat_id):
        if self.father:
            self.father.state[chat_id] = self.id

    def print_status(self, chat_id):
        current_inner = self.main_conversation
        inner_stage = current_inner.get_stage(chat_id)
        print("<-->", current_inner.id)
        tab = "     "

        while current_inner.object_type == "Conversation" and inner_stage:
            print(tab, "-->", inner_stage)
            tab += "    "

            current_inner = current_inner.get_inner_object(inner_stage)
            if current_inner.object_type != "Conversation":
                break
            inner_stage = current_inner.get_stage(chat_id)

        # print(tab, "<-->", current_inner.id, inner_stage, current_inner.object_type)

    @property
    def bot(self) -> telepot.Bot:
        return self.context.bot

    @property
    def main_conversation(self):
        if not self.father:
            return self
        main_conv = self.father
        while main_conv.father:
            main_conv = main_conv.father
        return main_conv

    @property
    def id(self):
        return str(self).split(" ")[0].split(".")[-1]
