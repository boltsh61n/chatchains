# ChatChains

Telepot based library for building  & handling complex bot chats.

## Installation

Use the git consol to clone the project from [gitlab](https://gitlab.com/boltsh61n/chatchains).

```bash
git clone https://gitlab.com/boltsh61n/chatchains
```

## Usage
An example of a simple calculator bot.
It contains one conversation that is also the main conversation, and three questions (first number, symbol, second number).
```python
import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from V1.ContextBase import ChainContext
from V1.Conversation.MainConversationBase import MainConversationBase
from V1.Question.QuestionBase import Question

MathSymbols = ["+", "-", "*", "/"]


class FirstNumber(Question):
    def ask(self, chat_id):
        self.set_page(chat_id, "Enter first number")

    def handle_input(self, chat_id, message: str, callback, files=None):
        if message:
            self.cache(chat_id, "number1", message)
            self.father.start_inner(chat_id, "MathSymbol")


class MathSymbol(Question):
    def ask(self, chat_id):
        self.set_page(chat_id, "choose math symbol",
                      InlineKeyboardMarkup(inline_keyboard=[
                          [InlineKeyboardButton(text=x, callback_data=f"/{x}") for x in MathSymbols]]
                      ))

    def handle_input(self, chat_id, message: str, callback, files=None):
        if callback:
            symbol = callback[1:]
            if symbol in ["+", "-", "*", "/"]:
                self.cache(chat_id, "math_symbol", symbol)
                self.father.start_inner(chat_id, "SecondNumber")
            else:
                self.set_page(chat_id, "incorrect input", sleep=3)


class SecondNumber(Question):
    def ask(self, chat_id):
        self.set_page(chat_id, "Enter second number")

    def handle_input(self, chat_id, message: str, callback, files=None):
        if message:
            number1 = self.cache(chat_id, "number1")
            symbol = self.cache(chat_id, "math_symbol")
            number2 = message

            res = "Failed to calculate.."
            if number1.isdigit() and number2.isdigit() and symbol in MathSymbols:
                res = eval(number1 + symbol + number2)

            self.set_page(chat_id, res, sleep=3)
            self.father.start(chat_id)


class CalculatorConversation(MainConversationBase):
    inner_objects = [
        FirstNumber,
        MathSymbol,
        SecondNumber
    ]


TOKEN = input("Enter bot token.\n>>>")

context = ChainContext(telepot.Bot(TOKEN), True)
main_conversation = CalculatorConversation(context=context)
main_conversation.run()

```


## License
For open source projects, say how it is licensed.

## Project status
Currently the system is in its first version and it dose'nt support media 


